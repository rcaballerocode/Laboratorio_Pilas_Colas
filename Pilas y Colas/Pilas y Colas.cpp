// Pilas y Colas.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include "stdafx.h"

using namespace std;
using namespace System;

struct paquete{
	int codigo;
	char identificador;
};

struct nodo{
	paquete Paquete;
	nodo *sgte;
};

typedef nodo* pila;
typedef nodo* cola;

pila pilaPrincipal = NULL;
cola colaLima = NULL;
cola colaProvincia = NULL;

void menu();
void recibirPaquete();
void distribuirPaquete();
void despachar(cola &miCola, char identificador);
void mostrar_pila();
void mostrar_cola(cola miCola, char destino);
void push_pila(pila &miPila, paquete pNuevo);
nodo* pop_pila(pila &miPila);
void push_cola(cola &miCola, nodo * nuevo);
void pop_cola(cola &miCola, char);
pila invertirPila(pila pilaAuxiliar);
void limpiarBuffer();

int cantPaquetes = 1, cantColaLima = 0, cantColaProvincia = 0, opcion = 0;

int _tmain(int argc, _TCHAR* argv[]){
	menu();
}

void menu(){
	opcion = 0;	
	system("cls");	
	cout << endl;
	cout << "\t\t\t������������������������������������ͻ" << endl;
	cout << "\t\t\t�                MENU                �" << endl;
	cout << "\t\t\t������������������������������������͹" << endl;
	cout << "\t\t\t�  1. Recibir paquete                �" << endl;
	cout << "\t\t\t�  2. Distribuir paquetes            �" << endl;
	cout << "\t\t\t�  3. Despachar paquetes (Lima)      �" << endl;
	cout << "\t\t\t�  4. Despachar paquetes (Provincia) �" << endl;
	cout << "\t\t\t�  5. Mostrar elementos              �" << endl;
	cout << "\t\t\t�  6. Salir                          �" << endl;
	cout << "\t\t\t������������������������������������ͼ" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";	
	
	cin >> opcion;

	switch (opcion){
	case 1: recibirPaquete(); break;
	case 2: distribuirPaquete(); break;
	case 3: despachar(colaLima, 'L'); break;
	case 4:	despachar(colaProvincia, 'P'); break;
	case 5: mostrar_pila();
		cout << "\n\tElementos cola Lima: " << endl;
		mostrar_cola(colaLima, 'L');
		cout << "\n\tElementos cola Provincia: " << endl;
		mostrar_cola(colaProvincia, 'P');
		limpiarBuffer(); menu(); break;
	case 6:	system("exit");  break;
	default: cout << "\n\t\t\t\tOpcion invalida!";		
		limpiarBuffer();
		menu(); break;
	}
}

void limpiarBuffer(){
	system("pause > nul");
	std::cin.clear();
	std::cin.ignore(INT_MAX, '\n');
}

void recibirPaquete(){
	paquete pNuevo;
	char iden;
	cout << "\n\t\tIngrese destino (L / P): ";
	cin >> iden;

	try{
		if (iden == 'L' || iden == 'P'){
			pNuevo.identificador = iden;
			pNuevo.codigo = cantPaquetes;
			cantPaquetes++;
			push_pila(pilaPrincipal, pNuevo);
			cout << endl << "El paquete con identificador: " << cantPaquetes - 1
				 << " y con destino: " << iden
				 << " se ha agregado correctamente" << endl;
			system("pause > nul");
		}
		else{
			cout << "\t\tDestino no valido!" << endl;
			system("pause > nul");
		}
	}
	catch (const char* msg){
		cerr << msg << endl;
	}
	menu();
}

void push_pila(pila &pilaPrincipal, paquete pNuevo){
	pila nuevoNodo = new (nodo);
	nuevoNodo->Paquete = pNuevo;
	nuevoNodo->sgte = NULL;

	if (pilaPrincipal == NULL){
		pilaPrincipal = nuevoNodo;
	}
	else{
		nuevoNodo->sgte = pilaPrincipal;
		pilaPrincipal = nuevoNodo;
	}
}

void distribuirPaquete(){
	nodo* nodoExtraido = pop_pila(pilaPrincipal);

	try {
		if (nodoExtraido == NULL){
			cout << "\n\t\tNo hay elementos en la pila para distribuir";
			system("pause > nul");
			menu();
		}
		else{
			if ((nodoExtraido->Paquete).identificador == 'L'){
				push_cola(colaLima, nodoExtraido);
			}
			else if ((nodoExtraido->Paquete).identificador == 'P'){
				push_cola(colaProvincia, nodoExtraido);
			}
			
			string destino = (nodoExtraido->Paquete).identificador == 'L' ? "Lima" : "Provincia";
			cout << "\n\t\tEl paquete se ha distribuido correctamente a " + destino + " ";
			system("pause > nul");
			menu();
		}
	}
	catch (const char* msg){
		cerr << msg << endl;
	}
}

nodo* pop_pila(pila &pilaPrincipal){
	if (pilaPrincipal == NULL)
		return NULL;
	else{
		pila aux = pilaPrincipal;
		pilaPrincipal = pilaPrincipal->sgte;
		aux->sgte = NULL;
		return aux;
	}
}

void push_cola(cola &colaDistribucion, nodo* nodoExtraido){
	if (colaDistribucion == NULL){
		colaDistribucion = nodoExtraido;
	}
	else{
		cola nodoAuxiliar = colaDistribucion;
		while (nodoAuxiliar->sgte != NULL)
			nodoAuxiliar = nodoAuxiliar->sgte;
		nodoAuxiliar->sgte = nodoExtraido;
	}	
	if ((nodoExtraido->Paquete).identificador == 'L')
		cantColaLima++;
	else
		cantColaProvincia++;
}

void despachar(cola &colaDistribucion, char destino){
	int cant;	
	char decision;

	if (colaDistribucion == NULL){
		cout << "\n   La cola de distribucion esta vacia, desea agregar un paquete? (s/n): ";
		cin >> decision;
		if (decision == 's' || decision == 'S'){
			recibirPaquete();
		}
		else {
			menu();
		}
	}
	else{		
		cout << "\n\tCuantos paquetes desea despachar?: ";
		cin >> cant;
		
		if (cin.fail()) {
			std::cin.clear();
			std::cin.ignore(INT_MAX, '\n');
			cout << "\tNumero de paquetes invalido!!!";
			system("pause > nul");
			menu();
		}
		
		if (cant > (destino == 'L' ? cantColaLima : cantColaProvincia)){
			cout << "Su pedido sobrepasa la cantidad de paquetes!" << endl;
		}
		else{
			for (int i = 0; i < cant; i++){
				pop_cola(colaDistribucion, destino);
			}
			cout << "\tPaquete(s) despachado(s)! ";
			system("pause > nul");
			menu();
		}
	}
}

void pop_cola(cola &colaDistribucion, char destino){
	cola nodoEliminado = colaDistribucion;
	colaDistribucion = colaDistribucion->sgte;
	delete nodoEliminado;

	if (destino == 'L'){
		cantColaLima--;
	}
	else{
		cantColaProvincia--;
	}
}

void mostrar_pila(){
	char buffer[50];

	nodo* pilaAuxiliar = pilaPrincipal;
	
	cout << "\n\t\t\t   Elementos de la pila:\n" << endl;

	if (pilaAuxiliar == NULL){
		cout << "\t\t\t   Pila vacia!" << endl;
	}
	else{
		string cadenaPila("\n\t\t\t\t      ��������ͼ");
		string cadenaElementos("");
		if (pilaAuxiliar != NULL){
			cadenaElementos.insert(0, "�  " + string(1,(pilaAuxiliar->Paquete).identificador) + 
				                   "/" + std::to_string((pilaAuxiliar->Paquete).codigo) + "   �      \t\t\t\t");
			pilaAuxiliar = pilaAuxiliar->sgte;

			while (pilaAuxiliar != NULL){
				cadenaElementos.insert(0, "�  " +
					                   string(1,(pilaAuxiliar->Paquete).identificador) +									  
									   "/" + std::to_string((pilaAuxiliar->Paquete).codigo) + "   �      \t\t\t\t" + "\n");
				pilaAuxiliar = pilaAuxiliar->sgte;
			}
			reverse(cadenaElementos.begin(), cadenaElementos.end());		
			cout << cadenaPila.insert(0, cadenaElementos) << endl;
		}
	}
}

void mostrar_cola(cola colaDistribucion, char destino){
	string destinoPaquete = destino == 'L' ? "Lima" : "Provincia";
	string cadenaColaSuperior = "";
	string cadenaColaDato = "";
	string cadenaColaInferior = "";
	
	if (colaDistribucion == NULL){		
		cout << "\tLa cola de distribucion a " << destinoPaquete << " esta vacia!!!" << endl;
	}
	else{
		nodo* nodoAuxiliar = colaDistribucion;	
		
		while (nodoAuxiliar != NULL){
			cadenaColaSuperior = cadenaColaSuperior + "��ͻ";
			cadenaColaDato = std::to_string((nodoAuxiliar->Paquete).codigo) + "/" +
				                            (nodoAuxiliar->Paquete).identificador + "�" + 
										    cadenaColaDato;
			cadenaColaInferior = cadenaColaInferior + "��ͼ";
			
			nodoAuxiliar = nodoAuxiliar->sgte;
		}
		cout << "\n\t" + cadenaColaSuperior << endl;
		cout << "\t" + cadenaColaDato << endl;
		cout << "\t" + cadenaColaInferior << endl;
	}
}